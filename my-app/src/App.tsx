import './App.css';
import SampleForm from './Container/Form';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import MainRouter from './Routers/MainRouter';

function App() {
  return (
    <div className="App">
      <MainRouter />
    </div>
  );
}

export default App;
