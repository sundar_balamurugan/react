import { Route, Redirect, RouteProps } from "react-router-dom"

interface PrivateRouteProps extends RouteProps {
    component: any;
}


function PrivateRoute(props: PrivateRouteProps) {
    const { component: Component, ...rest } = props;
    const loginFlag = localStorage.getItem('setlogin')
    return (
        <Route {...rest} render={(props) => loginFlag == 'true' ?
            <Component {...props} /> :
            <Redirect to='/login' />}
        />
    )
}

export default PrivateRoute;