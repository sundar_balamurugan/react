import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import SampleForm from '../Container/Form';
import List from '../Container/List';
import Login from '../Container/Login';
import PrivateRoute from './PrivateRoute';

function MainRouter(props: any) {
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/login' component={Login} />
                <PrivateRoute exact path='/' component={SampleForm} />
                <Route path='/list' component={List} />
            </Switch>
        </BrowserRouter>
    )
}

export default MainRouter;