import React from 'react';
import { Container, Form, Button } from 'react-bootstrap';

interface LoginState {
    username: string;
    password: string;
    flag: boolean;
}

class Login extends React.Component<{}, LoginState>{
    constructor(props: any) {
        super(props);
        this.state = {
            username: '',
            password: '',
            flag: false
        }
    }

    handleChange = (e: { target: { name: any; value: any }; }) => {
        const { name, value } = e.target
        this.setState({ [name]: value } as Pick<LoginState, keyof LoginState>)
    }

    handleSubmit = (e: any) => {
        localStorage.setItem('Setlogin', JSON.stringify(true))
    }

    render() {
        return (
            <div>
                <Container>
                    <h3>Login Form</h3>

                    <Form>
                        <Form.Group controlId="formGridEmail">
                            <Form.Label>username</Form.Label>
                            <Form.Control type="email" placeholder="Enter email"
                                name='username' value={this.state.username}
                                onChange={this.handleChange}
                            />
                        </Form.Group>

                        <Form.Group controlId="formGridPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password"
                                name='password' value={this.state.password}
                                onChange={this.handleChange} />
                        </Form.Group>
                        <Button variant="primary" type="submit" onClick={(e) => this.handleSubmit(e)}>
                            Login
                        </Button>
                    </Form>
                </Container>
            </div>
        )
    }
}

export default Login;
