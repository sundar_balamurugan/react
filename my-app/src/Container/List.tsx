import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';

interface user {
    id: number,
    firstname: string,
    lastname: string,
    mobile: number,
    password: string,
    pincode: string,
    addressline1: string,
    addressline2: string,
    state: string
    city: string
}

const List = () => {
    const [userData, setUserData] = useState<user[]>([]);
    useEffect(() => {
        axios.get<user[]>('https://60cad89c21337e0017e43279.mockapi.io/user')
            .then((response: AxiosResponse) => {
                console.log(response.data)
                setUserData(response.data)
            })
    }, [])
    console.log(userData)
    return (
        <div>
            {userData && userData.map((user, id) =>
                <div>
                    <h4>{id}</h4>
                    <p>{user.firstname}</p>
                    <p>{user.lastname}</p>
                    <p>{user.mobile}</p>
                    <p>{user.pincode}</p>
                    <p>{user.addressline1}</p>
                    <p>{user.state}</p>
                    <p>{user.city}</p>
                </div>
            )}
        </div>
    )
}

export default List;