import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

interface IProps extends RouteComponentProps<{ name: string }> {

}

interface IState {
    data: string
}

class Table extends React.Component<IProps, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: this.props.match.params.name
        }
    }
    render() {
        return (
            <div>
                <h1>{this.state.data}</h1>
            </div>
        )
    }
}

export default Table;