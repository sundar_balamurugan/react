
const initialState = {
    formData: {
        email: '',
        password: '',
        address1: '',
        address2: '',
        city: '',
        zip: '',
    }
}

interface Actiontype {
    type: 'Add'
    payload: {}
}

type Action = Actiontype;


const formReducer = (state: {} = initialState, action: Action) => {
    console.log(state, 'reducer', action.payload)
    switch (action.type) {

        case 'Add':
            console.log(state, 'reducer', action.payload)
            return {
                ...state,
                formData: action.payload
            }


        default: return state;
    }
}

export default formReducer;